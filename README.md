# Thonny log analyser
### Web application is available **[here](https://progtugi.cs.ut.ee/thonny-log-analyser/)**. 

---

To use web application locally, download the repository and open the repository folder in the browser.

This web application was developed for Rene Kütt's 2021 bachelor's thesis at the University of Tartu. 